import React from 'react'
import { StyleSheet, View, Text, Button, Image } from 'react-native'
import Colors from '../constants/colors'
import MainButton from '../components/MainButton'

const GameOverScreen = props => {
  return (
    <View style={styles.screen}>
      <Text>The Game is Over!</Text>
      <View style={styles.imageContainer}>
        <Image
          // source={require('../assets/vong.jpg')}
          source={{
            uri:
              'https://www.deccanherald.com/sites/dh/files/styles/article_detail/public/article_images/2020/05/19/666830-920153087-1522143562.jpg?itok=Hs349L-B'
          }}
          style={styles.image}
          resizeMode='cover'
        />
      </View>
      <View style={styles.resultContainer}>
        <Text style={styles.resultText}>
          Your phone needed{' '}
          <Text style={styles.highlight}>{props.roundsNumber} </Text>
          rounds to guess the number{' '}
          <Text style={styles.highlight}>{props.userNumber}</Text>
        </Text>
      </View>
      <MainButton onPress={props.onRestart}>New Game</MainButton>
    </View>
  )
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageContainer: {
    width: 300,
    height: 300,
    borderRadius: 1500,
    borderWidth: 3,
    borderColor: 'black',
    overflow: 'hidden',
    marginVertical: 30
  },
  image: {
    width: '100%',
    height: '100%'
  },
  resultContainer: {
    marginHorizontal: 20
  },
  highlight: {
    color: Colors.primary
  },
  resultText: {
    textAlign: 'center',
    fontSize: 18
  }
})

export default GameOverScreen
